import factory

from django.contrib.auth.models import User


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    username = 'staffuser'
    email = 'staff@company.com'
    password = factory.PostGenerationMethodCall(
        'set_password',
        'staffpass'
    )
    is_staff = True
    is_active = True
    is_superuser = True


class InactiveUserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    username = 'inactiveuser'
    email = 'inactiveuser@company.com'
    password = factory.PostGenerationMethodCall(
        'set_password',
        'inactiveuserpass'
    )
    is_staff = True
    is_active = False
    is_superuser = True
