from django.contrib import admin

from .models import Log


class LogAdmin(admin.ModelAdmin):
    list_display = (
        'message',
        'created_at',
    )
    search_fields = ['message']

admin.site.register(Log, LogAdmin)
