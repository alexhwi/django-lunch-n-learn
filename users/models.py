from django.db import models


class Log(models.Model):
    message = models.CharField(
        max_length=250,
        null=True,
        blank=True
    )
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.message
