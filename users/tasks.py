import time
from djangolunchnlearn.celery import app

from .models import Log


@app.task()
def hello_world(x):
    log_msg = 'Task completed.'
    Log.objects.create(
        message=log_msg
    )


@app.task()
def failed_login(username):
    log_msg = username + ' failed to login.'
    Log.objects.create(
        message=log_msg
    )


@app.task()
def test_scheduled_task():
    log_msg = 'Task' + ' created on schedule.'
    Log.objects.create(
        message=log_msg
    )


@app.task()
def add(x, y):
    return x + y


@app.task()
def step_one():
    print('Step 1')


@app.task(ignore_result=True)
def step_two(x):
    print('Step 2')


@app.task(ignore_result=True)
def step_three(x):
    print('Step 3')
