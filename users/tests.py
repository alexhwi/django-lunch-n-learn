from django.test import TestCase
from django.core.urlresolvers import reverse
from djangolunchnlearn.factories import UserFactory, InactiveUserFactory

from djangolunchnlearn.celery import app as celeryapp


class LoginViewTest(TestCase):

    def setUp(self):
        celeryapp.conf.update(CELERY_ALWAYS_EAGER=True)
        self.user = UserFactory()
        self.inactiveuser = InactiveUserFactory()
        self.url = reverse('users:login')

    def test_login_get(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('login.html')
        self.assertTrue('form' in response.context)

    def test_login_correctly(self):
        form_data = {
            'username': self.user.username,
            'password': 'staffpass'
        }
        response = self.client.post(
            self.url,
            form_data,
        )
        self.assertEqual(response.status_code, 302)

    def test_login_post_incomplete_data(self):
        form_data = {}
        self.assertTemplateUsed('login.html')
        response = self.client.post(
            self.url,
            form_data,
        )
        self.assertTrue('form' in response.context)
        self.assertFormError(
            response,
            'form',
            'username',
            'This field is required.'
        )
        self.assertFormError(
            response,
            'form',
            'password',
            'This field is required.'
        )
        expected = 'Ooops, please try again.'
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, expected)

    def test_login_wrong_user(self):
        form_data = {
            'username': 'wronguser',
            'password': 'wrongpassword'
        }
        response = self.client.post(
            self.url,
            form_data,
        )
        expected = 'Incorrect username or password.'
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, expected)
        self.assertTemplateUsed('login.html')

    def test_login_inactive_user(self):
        form_data = {
            'username': self.inactiveuser.username,
            'password': 'inactiveuserpass'
        }
        response = self.client.post(
            self.url,
            form_data,
        )
        expected = 'Incorrect username or password.'
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, expected)
        self.assertTemplateUsed('login.html')
