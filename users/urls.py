from django.conf.urls import url
from . import views as user_views


urlpatterns = [
    url(r'^login/$',
        user_views.LoginView.as_view(),
        name='login'),
]
