from django.contrib import messages
from django.contrib.messages import constants as msg


def show_error_message(self, message):
    messages.add_message(
        self.request,
        msg.ERROR,
        message
    )
