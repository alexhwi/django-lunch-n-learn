from django.shortcuts import render

from django.contrib.auth import (
    authenticate,
    login,
)
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView

from .forms import LoginForm
from .utils import show_error_message

from .tasks import (
    hello_world, 
    failed_login,
    step_one,
    step_two,
    step_three,
)


class LoginView(TemplateView):
    template_name = 'login.html'

    def get(self, request):
        form = LoginForm()
        x = 0
        while x < 1:
            hello_world.apply_async((x,))
            x += 1

        context = {
            'form': form,
        }
        return render(
            request,
            self.template_name,
            context
        )

    def post(self, request):
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return HttpResponseRedirect(
                    reverse(
                        'admin:index',
                    )
                )
            else:
                # failed_login.apply_async((username,), countdown=30)
                chain = step_one.s() | step_two.s() | step_three.s()
                chain()
                show_error_message(self, 'Incorrect username or password.')
        else:
            show_error_message(self, 'Ooops, please try again.')

        context = {
            'form': form,
        }
        return render(
            request,
            self.template_name,
            context
        )
